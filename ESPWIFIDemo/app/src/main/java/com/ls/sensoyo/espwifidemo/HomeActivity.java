package com.ls.sensoyo.espwifidemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    String IP = null;
    int Port = 0;
    TCPStrClient mTcpClient;
    public ProgressDialog progress;
    EditText etHomeIP,etHomePort,editMessage;
    Button btnHomeConnect,buttonSend,buttonGetList,btnHomeDisconnect;
    RecyclerView wifiRecylerList;
    TextView txtReceive;
    ScrollView SCROLLER_ID;
    LinearLayout ll_send_message;
    connectTask connect_Task;
    public AlertDialog alertDialog;
    public static final String CRLF = "\r\n";
    public static final String H_CR = "0d";
    public static final String H_CRLF = "0d0a";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        etHomeIP = findViewById(R.id.etHomeIP);

        btnHomeDisconnect = findViewById(R.id.btnHomeDisconnect);
        etHomePort = findViewById(R.id.etHomePort);
        btnHomeConnect = findViewById(R.id.btnHomeConnect);
        editMessage= findViewById(R.id.editMessage);
        buttonSend= findViewById(R.id.buttonSend);
        buttonGetList = findViewById(R.id.buttonGetList);
        wifiRecylerList= findViewById(R.id.wifiRecylerList);
        txtReceive= findViewById(R.id.txtReceive);
        SCROLLER_ID= findViewById(R.id.SCROLLER_ID);
        ll_send_message = findViewById(R.id.ll_send_message);
        ll_send_message.setVisibility(View.VISIBLE);
        this.progress = new ProgressDialog(this);
        this.progress.setIndeterminate(true);
        this.progress.setCancelable(true);


        btnHomeDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Disconnect();
            }
        });

        buttonGetList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTcpClient == null) {
                    alert();
                } else if (mTcpClient.socket == null || !mTcpClient.socket.isConnected()) {
                    alert();
                }else{
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("get","wifi");
                        SendCommand(jsonObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        btnHomeConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect();
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("SSID","yrsdg");
                        jsonObject.put("PASS","123");

                        JSONObject jsonObject1 = new JSONObject();
                        jsonObject1.put("set",jsonObject);
                        SendCommand(jsonObject1.toString());



                   /* JSONArray jsonArray = new JSONArray();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("SSID","ABC");
                    jsonObject.put("RSSI",-10);

                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("SSID","DEF");
                    jsonObject1.put("RSSI",-11);
                    jsonArray.put(jsonObject);
                    jsonArray.put(jsonObject1);

                    JSONObject finalJSONobject= new JSONObject();
                    finalJSONobject.put("list", jsonArray);
                    SendCommand(finalJSONobject.toString());*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*if(!editMessage.getText().toString().trim().equals(""))
                   SendCommand(editMessage.getText().toString());*/
            }
        });



    }

    private void Disconnect() {
        TCPStrClient tCPClient = this.mTcpClient;
        if (tCPClient != null) {
            tCPClient.stopClient();
            if (this.mTcpClient.socket != null && this.mTcpClient.socket.isConnected()) {
                try {

                    this.mTcpClient.socket.close();
                    HomeActivity.this.etHomePort.setEnabled(true);
                    HomeActivity.this.etHomeIP.setEnabled(true);
                    HomeActivity.this.btnHomeConnect.setEnabled(true);
                    HomeActivity.this.btnHomeDisconnect.setEnabled(false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void connect() {
        Disconnect();
        this.IP = etHomeIP.getText().toString().trim();
        this.Port = Integer.parseInt(etHomePort.getText().toString().trim());
        this.connect_Task = new connectTask();
        this.connect_Task.execute(new String[]{""});
        ProgressDialog progressDialog = this.progress;
        progressDialog.setMessage("Connecting with " + this.IP);
        if (!this.progress.isShowing()) {
            this.progress.show();
        }
    }

    private class connectTask extends AsyncTask<String, String, TCPStrClient> {

        public connectTask() {
        }

        @Override
        protected TCPStrClient doInBackground(String... strings) {
            mTcpClient = new TCPStrClient(connectTask.this::publishProgress,HomeActivity.this.IP, HomeActivity.this.Port);
            mTcpClient.run();
            return null;
        }

        @Override
        protected void onProgressUpdate(String ... values) {
            super.onProgressUpdate(values);



            if (HomeActivity.this.progress.isShowing()) {
                HomeActivity.this.progress.dismiss();
            }
            HomeActivity.this.txtReceive.append(values[0]);
            HomeActivity.this.txtReceive.append(CRLF);
            HomeActivity.this.SCROLLER_ID.fullScroll(130);
            if(values.length!=0){
                try {
                    String json = values[0];
                    if(json.equals("Connected to Server")){
                        HomeActivity.this.etHomePort.setEnabled(false);
                        HomeActivity.this.etHomeIP.setEnabled(false);
                        HomeActivity.this.btnHomeConnect.setEnabled(false);
                        HomeActivity.this.btnHomeDisconnect.setEnabled(true);
                    }
                    Log.d("My App", json);
                    json = json.replace("Message from server: ","");
                    Log.d("My App1", json);
                    JSONObject responseObject = new JSONObject(json);
                    Log.d("My App", responseObject.toString());
                    if(responseObject.has("list")){
                        try {
                            JSONArray jsonArray = responseObject.getJSONArray("list");
                            List<WifiModel> wifiModels = new ArrayList<>();
                            for(int i =0;i< jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                WifiModel  wifiModel = new WifiModel();
                                wifiModel.setRSSI(jsonObject.getInt("RSSI"));
                                wifiModel.setSSID(jsonObject.getString("SSID"));
                                wifiModels.add(wifiModel);
                            }
                            if(wifiModels.size()!=0){
                                wifiRecylerList.setVisibility(View.VISIBLE);
                                showAvailableNetworks(wifiModels);
                            }else{
                                wifiRecylerList.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else if(responseObject.get("set").equals("write OK")){
                        wifiRecylerList.setVisibility(View.GONE);
                        Toast.makeText(HomeActivity.this,"Config set successfully",Toast.LENGTH_LONG).show();
                    }else{
                        if (HomeActivity.this.progress.isShowing()) {
                            HomeActivity.this.progress.dismiss();
                        }
                        HomeActivity.this.txtReceive.append(responseObject.toString());
                        HomeActivity.this.SCROLLER_ID.fullScroll(130);
                    }

                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON");
                }


            }

        }
    }


    private void showAvailableNetworks(List<WifiModel> scanResults) {
        wifiRecylerList.addItemDecoration(new DividerItemDecoration(HomeActivity.this, DividerItemDecoration.VERTICAL));
        wifiRecylerList.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
        wifiRecylerList.setAdapter(new WifiNetworksListAdapter(HomeActivity.this, scanResults));
    }


    List<WifiModel> getDeviceList(){
        List<WifiModel> wifiModels = new ArrayList<>();

        WifiModel  wifiModel = new WifiModel();
        wifiModel.setRSSI(-10);
        wifiModel.setSSID("Abc");
        wifiModels.add(wifiModel);

        WifiModel  wifiModel1 = new WifiModel();
        wifiModel1.setRSSI(-10);
        wifiModel1.setSSID("pfe");
        wifiModels.add(wifiModel1);

        WifiModel  wifiModel2 = new WifiModel();
        wifiModel2.setRSSI(-10);
        wifiModel2.setSSID("efg");
        wifiModels.add(wifiModel2);
        return wifiModels;
    }

    public void SendCommand(String message){

        Log.e("JSON","JSON=== " + message);
        if (mTcpClient == null) {
            alert();
        } else if (mTcpClient.socket == null || !mTcpClient.socket.isConnected()) {
            alert();
        }else{
            this.mTcpClient.sendMessage(message);
        }

    }

    public void alert() {
        AlertDialog alertDialog2 = this.alertDialog;
        if (alertDialog2 == null || !alertDialog2.isShowing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((CharSequence) "Alert!");
            builder.setMessage((CharSequence) "Device not Connected").setIcon((int) R.drawable.error).setPositiveButton((CharSequence) "OK", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            this.alertDialog = builder.create();
            this.alertDialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Confirmation");
        builder.setMessage((CharSequence) "Do you want to Terminate? ");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    if (HomeActivity.this.mTcpClient != null) {
                        HomeActivity.this.mTcpClient.stopClient();
                        HomeActivity.this.mTcpClient.socket.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                HomeActivity.this.finish();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }
}