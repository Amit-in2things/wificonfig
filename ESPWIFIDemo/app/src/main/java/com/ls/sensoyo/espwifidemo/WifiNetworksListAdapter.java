package com.ls.sensoyo.espwifidemo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class WifiNetworksListAdapter extends RecyclerView.Adapter<WifiNetworksListAdapter.WifiNetworkItemViewHolder>  {

    private HomeActivity context;
    private List<WifiModel> scanResults;


    public WifiNetworksListAdapter(HomeActivity context, List<WifiModel> scanResults) {
        this.context = context;
        this.scanResults = scanResults;
    }

    @NonNull
    @Override
    public WifiNetworkItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.wifi_network_item_view, parent, false);
        return new WifiNetworkItemViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final WifiNetworkItemViewHolder holder, int position) {
        holder.addWifiNetworkToList(scanResults.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final WifiModel scanResult = scanResults.get(holder.getAdapterPosition());
                // create an alert builder
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Config");
                // set the custom layout
                final View customLayout = context.getLayoutInflater().inflate(R.layout.customconfiglayout, null);
                EditText ssidText = customLayout.findViewById(R.id.edit_ssid);
                ssidText.setText(scanResult.getSSID());
                builder.setView(customLayout);
                // add a button
                builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // send data from the AlertDialog to the Activity
                        EditText ssidText = customLayout.findViewById(R.id.edit_ssid);
                        EditText passwordText = customLayout.findViewById(R.id.edit_password);
                        String ssid = ssidText.getText().toString();
                        String password = passwordText.getText().toString();
                        applyTexts(ssid,password);

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return scanResults.size();
    }

    public void applyTexts(String username, String password) {

        /*{
            "set":{
            "SSID":"HJW20310",//必选    required
                    "PASS":"ABCD1234",//必选     required
                    "mfr":"Elemental",//可选       optional
                    "type":"SanitiserDispenser",//可选 optional
                    "publish":"Quanto/test",//可选    optional
                    "subscribe":"Quanto/dev",//可选   optional
                    "mqttURL":"securemqtt.leapscale.net",//可选  optional
                    "mqttPORT":8883,//可选                       optional
                    "mqttUSER":"lsgpmqttq",//可选          optional
                    "mqttPASS":"ckA6P}T7tf'R",//可选      optional

        }
        }*/
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("SSID",username);
            jsonObject.put("PASS",password);

            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("set",jsonObject);
            context.SendCommand(jsonObject1.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    static class WifiNetworkItemViewHolder extends RecyclerView.ViewHolder {
        private final TextView wifiNetworkNameView;
        private TextView wifiNetworkRssiLevelView;

        WifiNetworkItemViewHolder(View itemView) {
            super(itemView);
            wifiNetworkNameView = itemView.findViewById(R.id.wifi_network_name);
            wifiNetworkRssiLevelView = itemView.findViewById(R.id.wifi_network_level);
        }

        void addWifiNetworkToList(WifiModel scanResult) {
            wifiNetworkNameView.setText(scanResult.SSID);
            wifiNetworkRssiLevelView.setText(String.valueOf(scanResult.RSSI));
        }
    }
}
