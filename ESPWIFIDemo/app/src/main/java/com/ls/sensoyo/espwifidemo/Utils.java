package com.ls.sensoyo.espwifidemo;

import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import java.util.regex.Pattern;

class Utils {
    private static final Pattern IP_ADDRESS = Pattern.compile("((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9]))");

    Utils() {
    }

    public static boolean isValidHostName(String str) {
        boolean z = false;
        if (str == null) {
            return false;
        }
        String trim = str.trim();
        if (!"".equals(trim) && trim.indexOf(" ") == -1) {
            boolean matches = Pattern.compile("[a-zA-Z0-9\\.\\-\\_]+").matcher(trim).matches();
            if (matches) {
                z = Pattern.compile("((.)*[a-zA-Z\\-\\_]+(.)*)+").matcher(trim.length() > 15 ? trim.substring(0, 15) : trim).matches();
            } else {
                z = matches;
            }
        }
        if (z || !IP_ADDRESS.matcher(trim).matches()) {
            return z;
        }
        return true;
    }

    static class InputFilterHex implements InputFilter {
        private final boolean mUpperCase = true;

        InputFilterHex() {
        }

        public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
            String str;
            int i5 = i2 - i;
            if (i5 <= 0) {
                return null;
            }
            char[] cArr = null;
            int i6 = 0;
            for (int i7 = i; i7 < i2; i7++) {
                char charAt = charSequence.charAt(i7);
                char upperCase = Character.toUpperCase(charAt);
                if (upperCase == 'A' || upperCase == 'B' || upperCase == 'C' || upperCase == 'D' || upperCase == 'E' || upperCase == 'F' || upperCase == '-' || Character.isDigit(charAt)) {
                    if ((this.mUpperCase && charAt != upperCase) || (!this.mUpperCase && charAt == upperCase)) {
                        if (cArr == null) {
                            cArr = new char[i5];
                            TextUtils.getChars(charSequence, i, i7, cArr, 0);
                        }
                        int i8 = i6 + 1;
                        if (!this.mUpperCase) {
                            upperCase = Character.toLowerCase(charAt);
                        }
                        cArr[i6] = upperCase;
                        i6 = i8;
                    } else if (cArr != null) {
                        cArr[i6] = upperCase;
                        i6++;
                    } else {
                        i6++;
                    }
                } else if (cArr == null) {
                    cArr = new char[i5];
                    TextUtils.getChars(charSequence, i, i7, cArr, 0);
                }
            }
            if (cArr == null) {
                return null;
            }
            if (i6 >= i5) {
                str = String.valueOf(cArr);
            } else {
                str = String.valueOf(cArr, 0, i6);
            }
            if (!(charSequence instanceof Spanned)) {
                return str;
            }
            SpannableString spannableString = new SpannableString(str);
            TextUtils.copySpansFrom((Spanned) charSequence, i, i6, (Class) null, spannableString, 0);
            return spannableString;
        }
    }
}
