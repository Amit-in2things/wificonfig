package com.ls.sensoyo.espwifidemo;

public class WifiModel {
    String SSID = null;
    int RSSI = -1;

    public WifiModel() {

    }

    public WifiModel(String SSID, int RSSI) {
        this.SSID = SSID;
        this.RSSI = RSSI;
    }

    public String getSSID() {
        return SSID;
    }

    public void setSSID(String SSID) {
        this.SSID = SSID;
    }

    public int getRSSI() {
        return RSSI;
    }

    public void setRSSI(int RSSI) {
        this.RSSI = RSSI;
    }

    @Override
    public String toString() {
        return "wifiModel{" +
                "SSID='" + SSID + '\'' +
                ", RSSI=" + RSSI +
                '}';
    }
}
